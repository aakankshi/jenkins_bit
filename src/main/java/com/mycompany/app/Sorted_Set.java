/*Create a sorted set of Employee objects.(Sort on Emp ID) 

Create a list of Employee objects & sort them by names in alphabetical 
order (may be same as above.) 
 */






import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

class Employee
{
	private int emp_ID;
	private String emp_name;
	private String ID;
	
	Employee(int emp_ID,String emp_name)
	{
		this.emp_ID=emp_ID;
		this.emp_name=emp_name;
		
	}

	public int getEmp_ID() {
		return emp_ID;
	}

	public void setEmp_ID(int emp_ID) {
		this.emp_ID = emp_ID;
	}

	public String getEmp_name() {
		return emp_name;
	}

	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}
	 public String toString(){

	        return "Name: "+this.emp_name+"-- ID: "+this.emp_ID;
 }
}
class Employee_ID implements Comparator<Employee>
{
	public int compare(Employee e1,Employee e2)
	{
		if(e1.getEmp_ID()>e2.getEmp_ID())
			return 1;
		else
			return -1;
	}
}

class Employee_Name implements Comparator<Employee>
{
	public int compare(Employee e1,Employee e2){
		return e1.getEmp_name().compareTo(e2.getEmp_name());
		
	}
}

public class Sorted_Set {
public static void main(String[] args)
{
	SortedSet<Employee> s=new TreeSet<Employee>(new Employee_ID());
	s.add(new Employee(101,"Aashish"));
	s.add(new Employee(110,"Akhil"));
	s.add(new Employee(203,"Sunita"));
	s.add(new Employee(401,"Aakankshi"));	
	
	/*Iterator itr=s.iterator();
	while(itr.hasNext())
	{
		System.out.println(itr.next());
	}*/
	for(Employee e:s){

        System.out.println(e);

    }
	SortedSet<Employee> n=new TreeSet<Employee>(new Employee_Name());
	n.add(new Employee(101,"Aashish"));
	n.add(new Employee(110,"Akhil"));
	n.add(new Employee(203,"Sunita"));
	n.add(new Employee(401,"Aakankshi"));	
	
	/*Iterator itr=s.iterator();
	while(itr.hasNext())
	{
		System.out.println(itr.next());
	}*/
	for(Employee e:n){

        System.out.println(e);
}
}	
}
